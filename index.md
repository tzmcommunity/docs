---
title: TZMCommunity docs
layout: default
heading: TZMCommunity docs

---
# TZMCommunity

Github: [https://github.com/TZMCommunity/](https://github.com/TZMCommunity/ "https://github.com/TZMCommunity/")

TZM Developers Telegram group: [https://t.me/joinchat/HJFMZkrHRwNOSYVZAqDCIQ](https://t.me/joinchat/HJFMZkrHRwNOSYVZAqDCIQ "https://t.me/joinchat/HJFMZkrHRwNOSYVZAqDCIQ")

\#tzm_developers at TZM Discord server: [https://discord.gg/SFhbzu2](https://discord.gg/SFhbzu2 "https://discord.gg/SFhbzu2")

# About tzm.community websites

This project is an effort to make creating new robust chapter websites (and other websites too) easy. 

Typically chapters are using Wordpress to host a website. The problem with that is that usually the sites created are slow to load and you will have to take care of security and plugin updates.

With tzm.community websites the approach is different. We are storing the config files of sites to version control (currently Github) and Jekyll is used to create a static website that is then hosted on a server. Hosting static files is fast and cheap.  

## How it works

1. You ask for a copy of the template and a sub domain. 
2. Edit the site with for example forestry.io (we are looking for open source tools to do this)
3. When everything is ready site can be published on you domain too.

# Pages

{% include pageindex.html %}